# P-Script-openrc

Özel boot script yapmak için servis

Boot sırasında başlatılacak olan bu servis p-start betiğini çalıştıracaktır. 

Servisi durdurmaya kalktığınızda ise p-stop çalışacaktır. 

Diğer betikler için kaynak kodu inceleyiniz...

# Kurulum

`make install` komutunu kullanın
